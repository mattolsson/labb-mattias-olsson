//
//  main.m
//  Labb
//
//  Created by MattiasO on 2015-01-22.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
