//
//  ViewController2.m
//  Labb
//
//  Created by MattiasO on 2015-01-23.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()
@property (weak, nonatomic) IBOutlet UIButton *red;
@property (weak, nonatomic) IBOutlet UIButton *orange;
@property (weak, nonatomic) IBOutlet UIButton *green;
@property (weak, nonatomic) IBOutlet UIButton *blue;

@end

@implementation ViewController2


- (IBAction)redButton:(id)sender {
    self.view.backgroundColor = [UIColor redColor];
}
- (IBAction)orangeButton:(id)sender {
    self.view.backgroundColor = [UIColor orangeColor];
}
- (IBAction)greenButton:(id)sender {
    self.view.backgroundColor = [UIColor greenColor];
}
- (IBAction)blueButton:(id)sender {
    self.view.backgroundColor = [UIColor blueColor];
}


- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
